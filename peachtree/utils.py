from peachtree.logutil import logger
import aiohttp
import disnake

api_url = "https://tinyurl.com/api-create.php"
error_icon_url = "https://em-content.zobj.net/source/twitter/376/prohibited_1f6ab.png"


async def create_short_url(url: str):
    async with aiohttp.ClientSession() as session:
        params = {"url": url}
        async with session.post(api_url, data=params) as response:
            logger.debug(f"Shortening URL: {url}")
            shortened_url = await response.text()
            logger.debug(f"Shortened URL: {shortened_url}")
            return shortened_url


def create_embed(interaction, bot, message: str, error: bool = False):
    color = disnake.Color.red() if error else disnake.Color.dark_theme()

    icon_url = error_icon_url if error else interaction.client.user.display_avatar.url

    embed = disnake.Embed(description=message, color=color)
    embed.set_author(name=bot.name)
    embed.set_thumbnail(url=icon_url)
    embed.set_footer(text=f"Made by {bot.author} using Peachtree")

    return embed


async def respond(
    interaction, bot, message: str, error: bool = False, ephemeral: bool = False
):
    logger.debug(f"Sending message: {message}. Error: {error}")
    embed = create_embed(interaction, bot, message, error)
    await interaction.response.send_message(embed=embed, ephemeral=ephemeral)
