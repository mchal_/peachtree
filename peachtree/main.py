import disnake
from disnake.ext import commands
from peachtree.logutil import logger
from peachtree.utils import create_short_url, respond


class Bot:
    def __init__(self, token, name, version, author):
        self.bot = commands.InteractionBot()
        self.token = token
        self.name = name
        self.version = version
        self.author = author

        @self.bot.event
        async def on_ready():
            logger.info(f"Logged in as {self.bot.user}")
            logger.info(
                f"In {len(self.bot.guilds)} servers, serving {len(self.bot.users)} users"
            )

            invite_link = await create_short_url(
                f"https://discordapp.com/oauth2/authorize?client_id={self.bot.user.id}&scope=bot"
            )

            logger.info(f"Invite link: {invite_link}")

        @self.bot.event
        async def on_slash_command(ctx):
            logger.info(
                f'Command `/{ctx.application_command.qualified_name}` invoked by "{ctx.author}" ({ctx.author.id}) in server "{ctx.guild.name}" ({ctx.guild.id})'
            )

        @self.bot.event
        async def on_slash_command_error(ctx, error):
            logger.error(
                f'Error in command `/{ctx.application_command.qualified_name}` invoked by "{ctx.author}" ({ctx.author.id}) in server "{ctx.guild.name}" ({ctx.guild.id}): [{type(error).__name__}: {error}]'
            )
            await respond(
                ctx,
                self,
                "Oops! Something went wrong. Please leave an issue with the following error and some "
                "information about what you were doing.\n```" + str(error) + "```",
                error=True,
                ephemeral=True,
            )

    def cogs(self, cogs):
        for cog in cogs:
            logger.debug(f"Loading cog: {cog}")
            self.bot.add_cog(cog)
            logger.debug(f"Loaded cog: {cog}")

    def start(self):
        self.bot.run(self.token)
