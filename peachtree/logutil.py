import logging
import os


def create_handler(handler_type, name=None, level=logging.DEBUG, formatter=None):
    handler = handler_type(name) if name else handler_type()
    handler.setLevel(level)
    handler.setFormatter(formatter)
    return handler


logger = logging.getLogger("peachtree")

if not logger.handlers:
    formatter = logging.Formatter("%(asctime)s [%(name)s] %(levelname)s - %(message)s")
    stream_handler = create_handler(logging.StreamHandler, None, formatter=formatter)
    file_handler = create_handler(
        logging.FileHandler, "peachtree.log", formatter=formatter
    )

    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)

loglevel = os.getenv("PEACHTREE_LOGLEVEL", "DEBUG")
numeric_level = getattr(logging, loglevel.upper(), None)

if not isinstance(numeric_level, int):
    raise ValueError(f"Invalid log level: {loglevel}")

logger.setLevel(numeric_level)
