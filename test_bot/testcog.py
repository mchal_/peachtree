from peachtree.main import commands, respond


class TestCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command()
    async def test(self, inter):
        await respond(inter, self.bot, "Hello, World!")

    @commands.slash_command()
    async def foobar(self, inter):
        await respond(inter, self.bot, f"{12 / 0}")
