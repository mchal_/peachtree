# Peachtree

A simple, extendable base for intent-based discord bots.

### What does it do?

Peachtree is intended to be used as a base to quickly and easily bootstrap a Discord bot. Be it for a niche purpose, or a general bot for your community, Peachtree saves you plenty of boring boilerplate and lets you actually write the behaviour and logic of the bot.

### Installation

The easiest way to install Peachtree is to add it to an existing poetry project like so:

```bash
$ poetry add git+https://gitlab.com/mchal_/peachtree
```

### Usage

The simplest usage of Peachtree is as follows:

```python
# main.py
from peachtree.main import Bot
from test_bot.testcog import TestCommands

testbot = Bot(
    name="The Foobar-inator 9000",
    version="10.3.5",
    token=os.environ["PEACHTREE_TOKEN"],
    author="Dee Veloper"
)

testbot.cogs([TestCommands(testbot)])
testbot.start()
```

Peachtree then handles the following:
- Printing info to the console
- Debug, info, and error logging
- Registering commands loaded through cogs
- Cleanly handling errors and reporting to the user

### Contributing

Peachtree is currently a steaming mess, so if you would like to help contribute, please feel free to make a merge request.
For any drastic changes, please open an issue first so we can discuss!

### License

[MIT](LICENSE)
